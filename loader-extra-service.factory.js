(function () {
    'use strict';

    // Function prepared to extend inside the loader template.
    function loaderExtraService () {
        return {

            extraByProject: function (contents, scope) {
                // factory to be extended by project to replace this one
            },

            extraByProjectWithContents: function (contents, scope) {
                // factory to be extended by project to replace this one
            }
        };
    }

    angular
        .module('bravoureAngularApp')
        .factory('loaderExtraService', loaderExtraService);

})();
