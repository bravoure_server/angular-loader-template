
# Loader Template - Bravoure Component

The loader template is used in the config.js (or special router generator file).

The default functionalities are created in locader-template-services.factory.js file,
and can be extended by project re creating a loader-template-services-extend.factory.js file
in jello_components_extended/angular-loader-template

for inserting a new functionality only "by project", creating a loader-extra-service.factory.js 
file in jello_components_extended/angular-loader-template


    ...
    template: '<loader-template></loader-template>',
    controller: function ....


### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-loader-template": "1.1"
      }
    }

and then run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
    

