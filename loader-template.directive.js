(function(){
    'use strict';

    function loaderTemplate (
        Restangular,
        $rootScope,
        $state,
        $location,
        PATH_CONFIG,
        metaTags,
        $translate,
        gtmScrollTrack,
        loaderTemplateServicesExtend,
        loaderExtraService
    ) {
        return {
            restrict: 'EA',
            replace: true,

            controller: function ($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

                // Adds the Event Scroll functionality
                gtmScrollTrack();

            },
            compile: function (){
                return {
                    pre: function preLink(scope, iElement, iAttrs) {

                        $rootScope.loadedElements = [];

                        var loc = $location.$$path.split('/');
                        var stateUrl = $state.params.url;
                        var url = stateUrl + $rootScope.previewHash;
                        var params = {};

                        if (typeof stateUrl != 'undefined' && stateUrl.indexOf('{') != -1) {
                            // id replacement
                            url  = (typeof loc[4] != 'undefined') ? url.replace('{id}', loc[4]) : url ;
                            url  = (typeof loc[4] != 'undefined') ? url.replace('{landing_page_id}', loc[4]) : url ;
                            // slug replacement
                            params = (typeof loc[3] != 'undefined') ? { slug: loc[3]} : '';

                        }

                        if (!angular.isUndefined(scope.modulesController)) {

                            // Factory to extend by project for adding extra functionalities when loading the template
                            loaderExtraService.extraByProject(scope.modulesController, scope);

                            // Shows the content for prerender
                            loaderTemplateServicesExtend.headerContentPrerender(scope.modulesController, scope);

                            // Loads the blocks Listener
                            loaderTemplateServicesExtend.loadBlocksListener(scope.modulesController, scope);

                            /** TEMPLATE - Defines the template **/
                            $rootScope.templateFile = (typeof scope.modulesController.content.template != 'undefined') ? scope.modulesController.content.template : 'template_1';
                            scope.template = PATH_CONFIG.EXTEND + 'templates/' + scope.templateFile + '.template.html';

                            /** CONTENT - Sets the variable for the content of the page **/
                            scope.content = scope.modulesController;

                            /** ANIMATIONS - Enables animations **/
                            $rootScope.enableAnim = true;

                            /* Prerender status */
                            loaderTemplateServicesExtend.setPrerender();

                            if (data.pagesToNotBeRedirected.indexOf($state.params.identifier) == -1) {

                                /* I18N urls */
                                loaderTemplateServicesExtend.i18nUrlsCheck(scope.modulesController);

                                /* Alternate urls */
                                loaderTemplateServicesExtend.alternateUrlsCheck(scope.modulesController);

                                /* Slug check */
                                loaderTemplateServicesExtend.slugCheck(scope.modulesController);

                            }

                            /* Indexation */
                            loaderTemplateServicesExtend.indexation(scope.modulesController);

                            // Updates the meta Tags with info from the API
                            metaTags.updateMetaTags(scope);

                            $rootScope.lang = $translate.use();


                        } else if (!angular.isUndefined(url)) {

                            Restangular.allUrl($state.params.identifier, host + url, params).getList().then(function (contents) {

                                if (typeof contents[0] == 'undefined') {

                                    // disables Animations
                                    $rootScope.enableAnim = false;

                                    // if the Page Ajax call doesn't work, it redirects to the 404 page
                                    $state.go(PATH_CONFIG.ERROR_IDENTIFIER, null, {
                                        location: false
                                    });

                                    $rootScope.preRenderStatus = 404;

                                } else {

                                    // Factory to extend by project for adding extra functionalities when loading the template
                                    loaderExtraService.extraByProject(contents, scope);

                                    // Shows the content for prerender
                                    loaderTemplateServicesExtend.headerContentPrerender(contents, scope);

                                    // Loads the blocks Listener
                                    loaderTemplateServicesExtend.loadBlocksListener(contents, scope);

                                    /** TEMPLATE - Defines the template **/
                                    $rootScope.templateFile = (typeof contents[0].content.template != 'undefined') ? contents[0].content.template : 'template_1';
                                    scope.template = PATH_CONFIG.EXTEND + 'templates/' + scope.templateFile + '.template.html';

                                    /** CONTENT - Sets the variable for the content of the page **/
                                    scope.content = contents[0];

                                    /** ANIMATIONS - Enables animations **/
                                    $rootScope.enableAnim = true;

                                    /* Prerender status */
                                    loaderTemplateServicesExtend.setPrerender();

                                    if (data.pagesToNotBeRedirected.indexOf($state.params.identifier) == -1) {

                                        /* I18N urls */
                                        loaderTemplateServicesExtend.i18nUrlsCheck(contents);

                                        /* Alternate urls */
                                        loaderTemplateServicesExtend.alternateUrlsCheck(contents);

                                        /* Slug check */
                                        loaderTemplateServicesExtend.slugCheck(contents);

                                    }

                                    /* Indexation */
                                    loaderTemplateServicesExtend.indexation(contents);

                                    // Updates the meta Tags with info from the API
                                    metaTags.updateMetaTags(scope);

                                    $rootScope.lang = $translate.use();

                                }
                            }, function () {

                                // disables Animations
                                $rootScope.enableAnim = false;

                                // if the Page Ajax call doesn't work, it redirects to the 404 page
                                $state.go(PATH_CONFIG.ERROR_IDENTIFIER, null, {
                                    location: false
                                });

                                $rootScope.preRenderStatus = 404;

                            });
                        }
                    }
                }
            },
            template: '<div ng-include="template"></div>'
        }
    }

    loaderTemplate.$inject = [
        'Restangular',
        '$rootScope',
        '$state',
        '$location',
        'PATH_CONFIG',
        'metaTags',
        '$translate',
        'gtmScrollTrack',
        'loaderTemplateServicesExtend',
        'loaderExtraService'
    ];

    angular
      .module('bravoureAngularApp')
      .directive('loaderTemplate', loaderTemplate);

})();
