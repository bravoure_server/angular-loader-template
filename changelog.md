## Angular Loader Template component / Bravoure component

This Component is used for loading the templates.

None of them are required


### **Versions:**

## [1.2.0] - 2018-04-03 
### Fixed
- Added Image header check and loading blocks after it.
- Updating loading-template-services with headerContentPrerender & loadBlocksListener methods
    In the header-template-n directives, when the header images are shown, also teh blocks are loaded using the 
    Image Check service, and also using a variable in the header (showHeader).


---------------------

1.1.5  - Added the pagesToNotBeRedirected option to exclude pages from being redirected to the router
1.1.4  - Added landing_single_pages slug check in
1.1.3  - Added scope to the extended version
1.1.2   - Fixed bug for items that the preview_hash was not working
1.1.1   - Removed social icons $rootScope variable
1.1     - Indexation, preventing the check in landing Subpages 
1.0.9   - Added previewHash Variable to be added to urls on ajax calls
1.0.8   - Added indexation to the loader template and to the service
1.0.6   - fix for template file not being set on the rootscope
1.0.7   - extra content loaded moved location
1.0.5   - Added extraByProjectWithContents function to extend the template with content
1.0.4   - Check that the redirection is only happening if it is not read by prerender
1.0.2   - Adds the services to the loader-template
1.0.1   - Adding loader-extra-service.factory.js to extend extra funcitonalities by project
1.0     - Initial version

---------------------
