(function () {
    'use strict';

    function loaderTemplateServices (
        $rootScope,
        $location,
        $state,
        PATH_CONFIG,
        $timeout
    ) {

        return {
            /* Prerender status */
            setPrerender: function () {

                if ($rootScope.preRender301Status) {
                    /** 301 Prerender - Sets the Pre Render Status code **/
                    $rootScope.preRenderStatus = 301;
                    $rootScope.preRender301Status = false;

                    if ($rootScope.preRenderHeader == '' || $rootScope.preRenderHeader == null) {
                        $rootScope.preRenderHeader = 'Location: ' + $location.$$absUrl;
                    }

                } else if ($rootScope.preRender302Status) {
                    /** 302 Prerender - Sets the Pre Render Status code **/
                    $rootScope.preRenderStatus = 302;
                    $rootScope.preRender302Status = false;

                    if ($rootScope.preRenderHeader == '' || $rootScope.preRenderHeader == null) {
                        $rootScope.preRenderHeader = 'Location: ' + $location.$$absUrl;
                    }

                } else {
                    /** 404 Prerender - Sets the Pre Render Status code **/
                    $rootScope.preRenderStatus = ('top.' + $state.params.identifier == PATH_CONFIG.ERROR_IDENTIFIER) ? 404 : 200;
                }

            },

            /** Slug check */
            slugCheck: function (contents) {

                // Only acts for item pages
                if (($state.params.identifier.toLowerCase().indexOf('_page') == -1) || ($state.params.identifier == 'top.landing_single_pages')) {

                    if (typeof contents[0].content.url_slug != 'undefined' && $state.params.identifier != PATH_CONFIG.ERROR_IDENTIFIER) {
                        if ($location.$$path != '/' + contents[0].content.url_slug) {

                            /** 301 Prerender - Sets the Pre Render Status code **/
                            $rootScope.preRenderStatus = 301;
                            $rootScope.preRender301Status =  true;
                            $rootScope.preRenderHeader = 'Location: ' + $state.href('top.' + contents[0].content.handle, {
                                id: contents[0].id,
                                slug: contents[0].content.slug
                            }, {
                                absolute: true
                            });

                            // Don't execute redirect for Prerender
                            if (navigator.userAgent.toLowerCase().indexOf('prerender') == -1) {
                                $state.transitionTo('top.' + contents[0].content.handle, {
                                    id: contents[0].id,
                                    slug: contents[0].content.slug
                                }, {
                                    notify: false,
                                    location: 'replace'
                                });
                            }

                        }
                    }
                }
            },

            /** I18N urls **/
            i18nUrlsCheck: function (contents) {

                var content = contents[0] || contents;

                if (typeof content.content.i18n_urls != 'undefined') {
                    $rootScope.i18nUrls = content.content.i18n_urls;
                }
            },

            /** Alternate urls **/
            alternateUrlsCheck: function (contents) {

                var content = contents[0] || contents;

                if (typeof content.content.alternate_urls != 'undefined') {
                    $rootScope.alternate = content.content.alternate_urls;
                }
            },

            /** Indexation **/
            indexation: function (contents) {

                var content = contents[0] || contents;

                // Preventing adding check for indexation in landingSubpages
                if (content.type != 'landing-page') {

                    if (typeof content.content.disable_indexation != 'undefined' && content.content.disable_indexation == 1) {
                        $rootScope.robotsMetaContent = 'noindex, nofollow, noarchive';
                    } else {
                        $rootScope.robotsMetaContent = 'index, follow';
                    }
                }
            },

            headerContentPrerender: function (contents, scope) {
                if ($rootScope.isPrerenderAgent) {

                    $timeout (function () {

                        // Adding a class for showing the content and prevent hidding it (for prerender service)
                        $ ('body').addClass ('prerender-loaded-content loaded');

                        // if there is a preloader
                        if (data.preloader) {
                            $ ('#preloader').addClass ('invisible');
                        }

                        scope.template_blocks = contents[0].content.blocks;
                        scope.showTemplateContent = true;

                        $timeout (function () {
                            if (document.querySelector ('.background-image') != '' &&
                                document.querySelector ('.background-image') != undefined) {
                                lazySizes.loader.unveil (document.querySelector ('.background-image'));
                            }
                        },0);
                    },0);
                }
            },

            loadBlocksListener: function (contents, scope) {

                $rootScope.$emit('headerContent', contents);

                // oad the blocks once the image header was loaded : image-check.service.js
                var loadBlocks = $rootScope.$on ('loadBlocks', function () {

                    $timeout (function () {
                        scope.template_blocks = contents[0].content.blocks;

                        $ ('.header-template').addClass ('loaded-content');

                        scope.showTemplateContent = true;

                        $timeout (function () {
                            $ (document).trigger ('scroll');
                        }, 500);
                    }, 0);


                });

                // Destroys the Listener for loadBLocks
                scope.$on ('$destroy', function () {
                    loadBlocks ();
                });
            }
        }
    }

    loaderTemplateServices.$inject = [
        '$rootScope',
        '$location',
        '$state',
        'PATH_CONFIG',
        '$timeout'
    ];

    angular
        .module('bravoureAngularApp')
        .factory('loaderTemplateServices', loaderTemplateServices);

})();
