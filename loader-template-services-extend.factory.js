(function () {
    'use strict';

    function loaderTemplateServicesExtend (loaderTemplateServices) {
        // Extends the loaderTemplateServices
        var ExtendedService = loaderTemplateServices;

        return ExtendedService;
    }

    loaderTemplateServicesExtend.$inject = ['loaderTemplateServices'];

    angular
        .module('bravoureAngularApp')
        .factory('loaderTemplateServicesExtend', loaderTemplateServicesExtend);
})();